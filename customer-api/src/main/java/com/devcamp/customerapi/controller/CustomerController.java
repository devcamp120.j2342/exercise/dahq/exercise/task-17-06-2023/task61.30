package com.devcamp.customerapi.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customerapi.model.Customer;
import com.devcamp.customerapi.model.Order;
import com.devcamp.customerapi.model.Product;
import com.devcamp.customerapi.repository.CustomerRepository;
import com.devcamp.customerapi.repository.OrderRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CustomerController {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    OrderRepository orderRepository;

    @GetMapping("/devcamp-customer")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        try {
            List<Customer> customers = new ArrayList<Customer>();
            customerRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/devcamp-order")
    public ResponseEntity<Set<Order>> getAllOrderByIdCustomer(
            @RequestParam(required = true, name = "customerId") long id) {
        try {
            Customer customert = customerRepository.findById(id);
            if (customert != null) {
                return new ResponseEntity<>(customert.getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/devcamp-product")
    public ResponseEntity<Set<Product>> getAllProductByOrderId(
            @RequestParam(required = true, name = "orderId") long id) {
        try {
            Order order = orderRepository.findById(id);
            if (order != null) {
                return new ResponseEntity<>(order.getProducts(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
